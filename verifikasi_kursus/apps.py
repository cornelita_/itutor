from django.apps import AppConfig


class VerifikasiKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'verifikasi_kursus'
