from django.urls import path
from . import views

app_name = 'verifikasi_kursus'
urlpatterns = [
    path('list', views.show_all_request, name="pengajuan_url"),
]