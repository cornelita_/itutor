from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from buat_kursus.models import Kursus

@login_required(login_url='login_logout:login_url')
def show_all_request(request):
    if request.user.is_staff:
        response = {}
        
        if request.method == 'POST':
            try:
                id_kursus = request.POST['id_kursus']
                kursus = Kursus.objects.get(pk = id_kursus)
                status_verifikasi = request.POST['status_verifikasi']
                kursus.status_verifikasi = status_verifikasi
                kursus.save()
                if status_verifikasi == "1":
                    response['message_success'] = 'Berhasil memverifikasi kursus'
                else:
                    response['message_success'] = 'Kursus telah ditolak'
            except:
                if status_verifikasi == "1":
                    response['message_failed'] = 'Gagal memverifikasi kursus, silahkan coba lagi'
                else:
                    response['message_failed'] = 'Gagal mengubah status verifikasi kursus, silahkan coba lagi'
        
        response['data'] = Kursus.objects.filter(status_verifikasi = 0)
        return render(request, 'pengajuan-list.html', response)
    return redirect('landing_page')