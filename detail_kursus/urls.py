from django.urls import path
from . import views

app_name = 'detail_kursus'
urlpatterns = [
    path('<int:id_kursus>',views.detail_kursus,name='detail_kursus'),
]
