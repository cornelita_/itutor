from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from buat_kursus.models import Kategori, Kursus
from daftar_kursus.models import Pendaftaran
from register.models import NonAdmin

# Create your views here.
def detail_kursus(request, id_kursus):
    kursus = Kursus.objects.get(id=id_kursus)
    pendaftaran = Pendaftaran.objects.filter(kursus_id=id_kursus).filter(status_pendaftar=3)

    
    # print(kursus)
    print(pendaftaran)
    response = {}
    response['kursus'] = kursus
    response['pendaftaran'] = pendaftaran
    if request.user.is_authenticated:
        user = NonAdmin.objects.get(pengguna_ptr_id=request.user.id)
        response['pengguna'] = user
        print(user.is_pelajar)
        print(user.is_pengajar)
        pendaftaran_pelajar_yang_boleh = Pendaftaran.objects.filter(kursus_id=id_kursus).filter(status_pendaftar=2).filter(pelajar_id=request.user.id)
        response['pelajar_valid'] = pendaftaran_pelajar_yang_boleh

    # print('pelajar: ',user.is_pelajar)
    # print('pengajar: ',user.is_pengajar)

    if len(pendaftaran) != 0:
        rata_rating=0
        jumlah=0
        for r in pendaftaran:
            rata_rating+=r.rating
            jumlah+=1
        rata_rating = rata_rating/jumlah
        response['rata_rating'] = 'Rated ' + str(round(rata_rating,1)) +'/5'
    else:
        response['rata_rating'] = 'Belum ada rating'
        
    
    
    return render(request, "detail_kursus.html", response)

