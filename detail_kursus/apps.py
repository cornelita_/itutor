from django.apps import AppConfig


class DetailKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'detail_kursus'
