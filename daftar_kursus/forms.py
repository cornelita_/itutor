from django import forms
from django.forms import ModelForm, widgets
from .models import Pendaftaran

class DaftarKursusForm(ModelForm):
    class Meta:
        model = Pendaftaran
        fields = ('start_kursus', 'stop_kursus', 'harapan', 'materi', 'pelajar', 'kursus')
        widgets = {
            'start_kursus': forms.DateInput(attrs={'type':'date'}),
            'stop_kursus': forms.DateInput(attrs={'type':'date'}),
            'harapan': forms.Textarea(attrs={'rows':5, 'placeholder':'Maksimal 500 Karakter'}),
            'materi': forms.Textarea(attrs={'rows':5, 'placeholder':'Maksimal 500 Karakter'})
        }
