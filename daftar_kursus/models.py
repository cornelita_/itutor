from django.db import models
from django.db.models.fields import BooleanField, CharField, DateField, DecimalField, IntegerField
from buat_kursus import models as kursus_models
from register import models as register_models

class Pendaftaran(models.Model):
    '''
        0 = Menunggu perubahan status
        1 = Ditolak
        2 = Diterima
        3 = Selesai (sudah memberikan feedback)
    '''
    status_pendaftar = IntegerField(
        default=0
    )
    feedback = CharField(
        'feedback',
        max_length=500
    )
    rating = IntegerField(
        default=0
    )
    status_penyelesaian = BooleanField(
        default=False
    )
    kursus = models.ForeignKey(kursus_models.Kursus,on_delete=models.CASCADE)
    pelajar = models.ForeignKey(register_models.Pelajar, on_delete=models.CASCADE)
    materi = CharField(
        'Materi Kursus',
        max_length=500
    )
    harapan = CharField(
        'Harapan Kursus',
        max_length=500
    )
    start_kursus = DateField()
    stop_kursus = DateField()

