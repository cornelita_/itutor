from django.apps import AppConfig


class DaftarKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'daftar_kursus'
