from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from buat_kursus.models import Kursus
from register.models import Pelajar
from .forms import DaftarKursusForm
from datetime import date
from django.contrib.auth.decorators import login_required

@login_required(login_url='login_logout:login_url')
def daftar_kursus(request,id_kursus):
    kursus = Kursus.objects.get(id=id_kursus)
    pelajar = Pelajar.objects.get(nonadmin_ptr_id=request.user.id)
    form = DaftarKursusForm(initial={'pelajar':pelajar, 'kursus':kursus})
    messages = ""
    nama_kursus = kursus.nama_kursus
    if request.method == 'POST':
        form = DaftarKursusForm(request.POST, initial={'pelajar':pelajar, 'kursus':kursus})
        print(form.is_valid())
        if form.is_valid():
            print("Cleaned data:", form.cleaned_data)
            print(type(form.cleaned_data['start_kursus']))
            print(form.cleaned_data['start_kursus']<form.cleaned_data['stop_kursus'])
            date_now = date.today()
            if form.cleaned_data['start_kursus'] <= form.cleaned_data['stop_kursus'] and form.cleaned_data['start_kursus'] >= date_now:
                form.save()
                return HttpResponseRedirect(reverse('kursus_pelajar:kursus_pelajar'))
            else:
                messages = "Tanggal Selesai harus lebih besar daripada Tanggal Mulai dan dimulai dari hari ini atau lebih"
    return render(request, "formDaftarKursus.html", {'form':form, 'messages':messages, 'nama_kursus':nama_kursus, 'id_kursus':id_kursus})