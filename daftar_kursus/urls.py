from django.urls import path
from .views import daftar_kursus

app_name = 'daftar_kursus'
urlpatterns = [
    path('<int:id_kursus>', daftar_kursus, name='daftar_kursus'),
]