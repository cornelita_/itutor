from .models import Admin, Pelajar, Pengajar
from django.contrib.auth.forms import UserCreationForm
from django import forms

class RegistrationPelajarForm(UserCreationForm):
    class Meta:
        model = Pelajar
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'jenis_kelamin', 'tanggal_lahir', 'lokasi', 'is_pelajar')
        widgets = {
            'username': forms.TextInput(attrs={'placeholder':'Masukkan Username'}),
            'first_name': forms.TextInput(attrs={'placeholder':'Masukkan Nama Lengkap'}),
            'email': forms.TextInput(attrs={'type':'email','placeholder':'Contoh: example@example.com'}),
            'password1': forms.TextInput(attrs={'type':'password','placeholder':'Minimum 8 Karakter'}),
            'password2': forms.TextInput(attrs={'type':'password','placeholder':'Masukkan password kembali'}),
            'lokasi': forms.TextInput(attrs={'placeholder':'Masukkan lokasi (Contoh: Depok, Indonesia)'}),
            'tanggal_lahir': forms.DateInput(attrs={'type':'date'})
        }

class RegistrationPengajarForm(UserCreationForm):
    class Meta:
        model = Pengajar
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'jenis_kelamin', 'tanggal_lahir', 'lokasi', 'is_pengajar', 'pendidikan', 'pengalaman')
        widgets = {
            'username': forms.TextInput(attrs={'placeholder':'Masukkan Username'}),
            'first_name': forms.TextInput(attrs={'placeholder':'Masukkan Nama Lengkap'}),
            'email': forms.TextInput(attrs={'type':'email','placeholder':'Contoh: example@example.com'}),
            'password1': forms.TextInput(attrs={'type':'password','placeholder':'Minimum 8 Karakter'}),
            'password2': forms.TextInput(attrs={'type':'password','placeholder':'Masukkan password kembali'}),
            'lokasi': forms.TextInput(attrs={'placeholder':'Masukkan lokasi (Contoh: Depok, Indonesia)'}),
            'tanggal_lahir': forms.DateInput(attrs={'type':'date'}),
            'pendidikan': forms.Textarea(attrs={'rows':'4','placeholder':'Masukkan Pendidikan (Contoh: S1 Fasilkom UI - 2019)'}),
            'pengalaman': forms.Textarea(attrs={'rows':'4','placeholder':'Masukkan Pengalaman (Contoh: Asisten Dosen DDP1 - 2019)'})
        }

class RegistrationAdmin(UserCreationForm):
    class Meta:
        model = Admin
        fields = ('username', 'first_name', 'email', 'password1', 'password2', 'is_staff')