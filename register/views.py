from django.shortcuts import render, redirect
from .forms import RegistrationPelajarForm, RegistrationPengajarForm, RegistrationAdmin
from django.contrib.auth import login

def register(request):
    return render(request, "indexRegister.html")

def register_pelajar(request):
    form = RegistrationPelajarForm(initial={'is_pelajar':True})
    response = {'form': form, 'messages':''}
    if request.method == 'POST':
        form = RegistrationPelajarForm(request.POST, initial={'is_pelajar':True})
        response['form'] = form
        if form.is_valid():
            user = form.save()
            if user is not None:
                login(request, user)
                return redirect('landing_page')
            else:
                response['messages'] = 'Username yang Anda masukkan tidak tersedia, silakan masukkan username lain atau login jika sudah punya akun'
        else:
            response['messages'] = 'Gagal Mendaftar'
    return render(request, "formRegisterPelajar.html", response)

def register_pengajar(request):
    form = RegistrationPengajarForm(initial={'is_pengajar':True})
    response = {'form': form, 'messages':''}
    if request.method == 'POST':
        form = RegistrationPengajarForm(request.POST, initial={'is_pengajar':True})
        response['form'] = form
        if form.is_valid():
            user = form.save()
            if user is not None:
                print("user saved")
                login(request, user)
                return redirect('landing_page')
        else:
            print(form.errors)
            response['messages'] = 'Username yang Anda masukkan tidak tersedia, silakan masukkan username lain atau login jika sudah punya akun'
    return render(request, "formRegisterPengajar.html", response)

def register_admin(request):
    form = RegistrationAdmin(initial={'is_staff':True})
    response = {'form': form}
    if request.method == 'POST':
        form = RegistrationAdmin(request.POST, initial={'is_staff':True})
        response['form'] = form
        if form.is_valid():
            user = form.save()
            if user is not None:
                login(request, user)
                return redirect('verifikasi_kursus:pengajuan_url')
        else:
            response['message'] = 'Registration Failed'
    return render(request, "formRegisterAdmin.html", response)
