from django.urls import path
from .views import register_pelajar, register_pengajar, register, register_admin

app_name = 'register'
urlpatterns = [
    path('', register, name="register"),
    path('pelajar/', register_pelajar, name="register_pelajar"),
    path('pengajar/', register_pengajar, name="register_pengajar"),
    path('admin/', register_admin, name="register_admin"),
]