from django.db import models
from django.contrib.auth.models import User

# https://testdriven.io/blog/django-custom-user-model/

class Pengguna(User):
    def __str__(self):
        return self.username

class Admin(Pengguna):
    pass

class NonAdmin(Pengguna):
    SEX_CHOICES = [
        ('P', 'Perempuan'),
        ('L', 'Laki-Laki')
    ]
    jenis_kelamin = models.CharField(choices=SEX_CHOICES, max_length=1)
    tanggal_lahir = models.DateField(
        'Tanggal Lahir',
        help_text="DD/MM/YYYY",
        unique=False    
    )
    lokasi = models.CharField(
        'Lokasi',
        max_length=100,
        help_text='lokasi'
    )
    is_pelajar = models.BooleanField(default=False)
    is_pengajar = models.BooleanField(default=False)
    list_contact = models.ManyToManyField("self")

    @property
    def get_all_contact(self):
        return list(self.list_contact.all())

    def get_all_pesan_diterima(self, other):
        return self.pesan_diterima.filter(penerima = self, pengirim = other)

    def get_all_pesan_dikirim(self, other):
        return self.pesan_dikirim.filter(pengirim = self, penerima = other)

    def get_all_pesan(self, other):
        return self.get_all_pesan_diterima(other).union(self.get_all_pesan_dikirim(other)).order_by('waktu').reverse()

    def add_contact_if_absent(self, contact):
        self.list_contact.add(contact)

class Pengajar(NonAdmin):
    pendidikan = models.CharField(
        'Pendidikan',
        max_length=500
    )
    pengalaman = models.CharField(
        'Pengalaman',
        max_length=500
    )

class Pelajar(NonAdmin):
    pass
