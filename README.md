# About
[production](http://i-tutor-u.herokuapp.com/) | [staging](http://i-tutor-u-staging.herokuapp.com/)

## RPL B-12
1. Achmad Fikri Adidharma - 1906398692 
2. Basyira Sabita - 1906400034
3. Bima Bagas Raditya - 1906398585
4. Cornelita Lugita Santoso - 1906292995
5. Shanika Tysha Anqita - 1906305890

# Set Up

1. Buat env
```
python -m venv rpl-tk5
```

2. Activate env
```
rpl-tk5\Scripts\activate
```

3. Clone this project
```
git clone https://gitlab.com/cornelita_/itutor.git

cd itutor
```

4. Install all requirements
```
pip install -r requirements.txt
```

5. Coba runserver
```
python manage.py runserver
```

# Directory Structure
```
|-- itutor
    |-- <nama_app>
        |-- <default_django_app_folders>
        |-- templates
            |-- <html files>
        |-- static
            |-- css
                |-- <css/scss files>
            |-- script
                |-- <js files>
            |-- image
                |-- <image files>
        |-- <default_django_app_files>
        |-- urls.py
        |-- <other_files (if needed)>
```

# Catatan bersama

1. Bikin app baru
```
python manage.py startapp <nama_app>
```

2. Semua file html extends app-shell.html
```
{% extends 'app-shell.html' %}
```