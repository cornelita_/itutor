from django.urls import path
from .views import buat_kursus

app_name = 'buat_kursus'
urlpatterns = [
    path('', buat_kursus, name='buat_kursus'),
]