from django.apps import AppConfig


class BuatKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buat_kursus'
