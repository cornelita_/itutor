from django.db import models
from register import models as register_models
from django.contrib.postgres.fields import ArrayField
from multiselectfield import MultiSelectField

class Kursus(models.Model):
    DAY_CHOICES = [
        ('Senin', 'Senin'),
        ('Selasa', 'Selasa'),
        ('Rabu', 'Rabu'),
        ('Kamis', 'Kamis'),
        ('Jumat', 'Jumat'),
        ('Sabtu', 'Sabtu'),
        ('Minggu', 'Minggu')
        
    ]
    nama_kursus = models.CharField(
        'Nama Kursus',
        max_length=100,
        unique=True
    )
    deskripsi = models.CharField(
        'Deskripsi',
        max_length=500
    )
    status_verifikasi = models.IntegerField(
        default=0
    )
    lokasi = models.CharField(
        'Lokasi',
        max_length=100,
        unique=False
    )
    pengajar = models.ForeignKey(register_models.Pengajar,on_delete=models.CASCADE)
    jadwal = MultiSelectField(
            choices=DAY_CHOICES
        )
    kategori = models.ForeignKey('Kategori', on_delete=models.CASCADE)


class Kategori(models.Model):
    nama_kategori = models.CharField(
        'Nama Kategori',
        max_length=100
    )

    def __str__(self):
        return self.nama_kategori