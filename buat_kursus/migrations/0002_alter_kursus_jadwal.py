# Generated by Django 3.2.9 on 2021-12-03 16:07

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('buat_kursus', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kursus',
            name='jadwal',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('Sen', 'Senin'), ('Sel', 'Selasa'), ('Rab', 'Rabu'), ('Kam', 'Kamis'), ('Jum', 'Jumat'), ('Sab', 'Sabtu'), ('Min', 'Minggu')], max_length=27),
        ),
    ]
