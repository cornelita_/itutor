from django.shortcuts import redirect, render
from .models import Kursus, Kategori
from django.contrib import messages
from register.models import Pengajar
from .forms import BuatKursusForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

@login_required(login_url='login_logout:login_url')
def buat_kursus(request):
    try:
        pengajar = Pengajar.objects.get(nonadmin_ptr_id=request.user.id)
    except:
        pengajar = None
    if pengajar == None:
        return redirect('/')
    form = BuatKursusForm(initial={'pengajar': pengajar})
    print(type(form))
    message_nama = ""
    message_jadwal = ""
    if request.method == 'POST':
        form = BuatKursusForm(request.POST, initial={'pengajar': pengajar})
        try:
            nama_exist = Kursus.objects.get(nama_kursus = request.POST['nama_kursus'])
        except:
            nama_exist = None
        try:
            jadwal = request.POST['jadwal']
        except:
            jadwal = None
        print(form.is_valid())
        if form.is_valid():
            form.save()
            return redirect('kursus_pengajar:kursus_pengajar')
        else:
            if nama_exist != None:
                message_nama = "Nama Kursus Harus Unik"
            if jadwal == None:
                message_jadwal = "Anda Harus Memilih Hari Kursus"
    return render(request, "formBuatKursus.html", {'form':form, 'message_nama':message_nama, 'message_jadwal':message_jadwal})