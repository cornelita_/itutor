from django import forms
from django.forms import fields, widgets, ModelForm
from .models import Kursus

class BuatKursusForm(ModelForm):
    class Meta:
        DAY_CHOICES = [
            ('Sen', 'Senin'),
            ('Sel', 'Selasa'),
            ('Rab', 'Rabu'),
            ('Kam', 'Kamis'),
            ('Jum', 'Jumat'),
            ('Sab', 'Sabtu'),
            ('Min', 'Minggu')   
        ]
        model = Kursus
        fields = ('nama_kursus', 'deskripsi', 'lokasi', 'kategori', 'jadwal', 'pengajar')
        widgets = {
            'nama_kursus': forms.TextInput(attrs={'placeholder': 'Masukkan Nama Kursus'}), 
            'deskripsi': forms.Textarea(attrs={'rows':5, 'placeholder':'Masukkan Deskripsi Kursus'}), 
            'lokasi': forms.TextInput(attrs={'placeholder':'Masukkan Lokasi Kursus (Contoh: Depok, Indonesia)'}),
            # 'jadwal':forms.MultipleChoiceField(choices = DAY_CHOICES)
        }