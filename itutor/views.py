from django.shortcuts import render
from register.models import NonAdmin

def landing_page(request):
    response = {}
    if request.user.is_authenticated and not request.user.is_staff:
        non_admin = NonAdmin.objects.get(username=request.user.username)
        response['is_pengajar'] = non_admin.is_pengajar
        response['is_pelajar'] = non_admin.is_pelajar
    return render(request, "static_pages/index.html", response)
