"""itutor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import urls
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.landing_page, name="landing_page"),
    path('admin/', admin.site.urls),
    path('authenticate/', include('login_logout.urls')),
    path('buat-kursus/', include('buat_kursus.urls')),
    path('sunting-kursus/', include('sunting_kursus.urls')),
    path('daftar-kursus/', include('daftar_kursus.urls')),
    path('detail-kursus/',include('detail_kursus.urls')),
    path('buat-feedback/',include('feedback.urls')),
    path('pengajuan-kursus/',include('pengajuan_kursus.urls')),
    path('pesan/', include('pesan.urls')),
    path('kursus-pengajar/', include('kursus_pengajar.urls')),
    path('verifikasi/', include('verifikasi_kursus.urls')),
    path('cari-kursus/', include('carikursus.urls')),
    path('register/', include('register.urls')),
    path('kursus-pelajar/', include('kursus_pelajar.urls')),
    path('profile-pengguna/', include('profile_pengguna.urls')),
]
