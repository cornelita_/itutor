from django.urls import path
from . import views

app_name = 'pesan'
urlpatterns = [
    path('add-contact/<id_pengajar>', views.add_contact, name="add_contact_url"),
    path('contacts', views.show_all_contact, name="contacts_url"),
    path('detail/<username>', views.show_all_pesan, name="message_url"),
]