from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from register.models import NonAdmin
from pesan.models import Pesan

@login_required(login_url='login_logout:login_url')
def add_contact(request, id_pengajar):
    other = NonAdmin.objects.get(pk=id_pengajar)
    non_admin = NonAdmin.objects.get(pk=request.user.id)
    non_admin.add_contact_if_absent(other)
    return redirect('pesan:contacts_url')

@login_required(login_url='login_logout:login_url')
def show_all_contact(request, error=False):
    non_admin = NonAdmin.objects.get(pk=request.user.id)
    response = {'contacts': non_admin.get_all_contact}
    if error:
        response['notification'] = "Terjadi kegagalan pada sistem, silahkan coba lagi"
    return render(request, 'contact-list.html', response)

@login_required(login_url='login_logout:login_url')
def show_all_pesan(request, username):
    response = {'header_1': True, 'username': username}
    try:
        id = User.objects.get(username=username)
        other = NonAdmin.objects.get(pk=id)
        non_admin = NonAdmin.objects.get(pk=request.user.id)
        response['nama_lengkap'] = other.first_name
    except:
        if (request.method == 'GET'):
            show_all_contact(request, error=True)
    
    if request.method == 'POST':
        try:
            pesan = Pesan(pesan=request.POST['pesan'], penerima=other, pengirim=non_admin)
            pesan.save()
        except:
            response['notification'] = "Gagal mengirim pesan, silahkan coba lagi"
            response['pesan'] = request.POST['pesan']
    
    response['messages'] = non_admin.get_all_pesan(other)
    return render(request, 'pesan.html', response)
