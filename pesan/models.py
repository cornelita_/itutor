from django.db import models
from django.db.models.deletion import CASCADE
from register.models import NonAdmin

class Pesan(models.Model):
    pesan = models.TextField()
    waktu = models.DateTimeField(auto_now_add=True)
    pengirim = models.ForeignKey(NonAdmin, related_name='pesan_dikirim', on_delete=CASCADE)
    penerima = models.ForeignKey(NonAdmin, related_name='pesan_diterima', on_delete=CASCADE)

    def __str__(self):
        return self.pesan
