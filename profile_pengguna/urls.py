from django.urls import path
from .views import profile_pelajar,profile_pengajar, profile

app_name = 'profile_pengguna'
urlpatterns = [
    path('<int:id>', profile, name="profile"),
]