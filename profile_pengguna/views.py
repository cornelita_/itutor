from math import ceil
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from buat_kursus.models import Kursus
from daftar_kursus.models import Pendaftaran
from register.models import Pelajar, Pengajar, NonAdmin

# Create your views here.

@login_required
def profile(request, id):
    user = NonAdmin.objects.get(pengguna_ptr_id=id)
    if user.is_pelajar:
        return profile_pelajar(request, id)
    else:
        return profile_pengajar(request, id)

def profile_pelajar(request, id):
    pelajar = Pelajar.objects.get(nonadmin_ptr_id=id)
    context = {
        # status_pendaftar = 2  -> ACCEPTED state (masih menjadi pelajar di kursus X)
        # status_pendaftar = 3 -> DONE state (sudah menyelesaikan kursus X)
        'jumlah_kursus_diikuti' : Kursus.objects.filter(pendaftaran__pelajar_id = pelajar.id).filter(pendaftaran__status_pendaftar=2),
        'jumlah_kursus_pernah_ikut' : Kursus.objects.filter(pendaftaran__pelajar_id = pelajar.id).filter(pendaftaran__status_pendaftar=3),
        'pelajar' : pelajar
    }
    context['zipped1'] = zip(context['jumlah_kursus_diikuti'], get_ratings(context['jumlah_kursus_diikuti']))
    context['zipped2'] = zip(context['jumlah_kursus_pernah_ikut'], get_ratings(context['jumlah_kursus_pernah_ikut']))
    return render(request, "profilePelajar.html", context)

def profile_pengajar(request, id):
    pengajar = Pengajar.objects.get(nonadmin_ptr_id=id)
    context = {
        # Kursus yang ditampilkan harus sudah terverifikasi
        'jumlah_kursus_dimiliki' : Kursus.objects.filter(pengajar_id=pengajar.id).filter(status_verifikasi=1),
        'pengajar' : pengajar
    }

    context['zipped'] = zip(context['jumlah_kursus_dimiliki'], get_ratings(context['jumlah_kursus_dimiliki']))
    return render(request, "profilePengajar.html", context)

def get_ratings(kursuses):
    list_rating = []
    for i in kursuses:
        pendaftaran = Pendaftaran.objects.filter(kursus_id=i.id).filter(status_pendaftar=3)

        rata_rating = 0
        jumlah = 0
        for r in pendaftaran:
            rata_rating += r.rating
            jumlah += 1
        if jumlah != 0 :
            rata_rating = rata_rating / jumlah

        html = render_to_string(
            template_name="generateStar.html",
            context= {
                'int_range' : range(int(rata_rating)),
                'decimal_score' : rata_rating - int(rata_rating),
                'remaining_star' : range(5 - ceil(rata_rating))
            }
        )

        list_rating.append(html)
    return list_rating