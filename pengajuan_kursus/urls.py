from django.urls import path
from . import views

app_name = 'pengajuan_kursus'
urlpatterns = [
    path('<int:id_kursus>',views.pengajuan_kursus,name='pengajuan_kursus'),
    path('tolak/<int:id_pendaftaran>/<int:id_kursus>', views.tolak_pengajuan, name='tolak_pengajuan'),
    path('terima/<int:id_pendaftaran>/<int:id_kursus>', views.terima_pengajuan, name='terima_pengajuan'),
]