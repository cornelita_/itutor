from django.apps import AppConfig


class PengajuanKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pengajuan_kursus'
