from django.http import response
from django.http.response import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from buat_kursus.models import Kategori, Kursus
from daftar_kursus.models import Pendaftaran
from register.models import NonAdmin
from django.contrib.auth.decorators import login_required

@login_required(login_url='login_logout:login_url')
def pengajuan_kursus(request, id_kursus):
    kursus = Kursus.objects.get(id=id_kursus)
    pendaftaran = Pendaftaran.objects.filter(kursus_id=id_kursus)
    pengguna = NonAdmin.objects.get(id=request.user.id)
    response = {}
    response['kursus'] = kursus
    response['pendaftaran'] = pendaftaran
    # print(request.user)
    if pengguna.is_pengajar and kursus.pengajar_id == pengguna.pengguna_ptr_id:
        return render(request,'pengajuan_kursus.html',response)
    else:
        print(kursus.pengajar_id)
        print(pengguna.pengguna_ptr_id)
        print(pengguna.is_pengajar)
        return render(request, 'gagal.html')
    

def tolak_pengajuan(request, id_pendaftaran, id_kursus):
    pendaftaran_dia = Pendaftaran.objects.get(id=id_pendaftaran)
    pendaftaran_dia.status_pendaftar = 1
    pendaftaran_dia.save(update_fields=['status_pendaftar'])
    return redirect('pengajuan_kursus:pengajuan_kursus', id_kursus=id_kursus)

def terima_pengajuan(request, id_pendaftaran, id_kursus):
    pendaftaran_dia = Pendaftaran.objects.get(id=id_pendaftaran)
    pendaftaran_dia.status_pendaftar = 2
    pendaftaran_dia.save(update_fields=['status_pendaftar'])
    return redirect('pengajuan_kursus:pengajuan_kursus', id_kursus=id_kursus)

