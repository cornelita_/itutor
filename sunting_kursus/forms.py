from django import forms
from django.forms import fields, widgets, ModelForm
from buat_kursus.models import Kursus

class SuntingKursusForm(ModelForm):
    class Meta:
        DAY_CHOICES = [
            ('Senin', 'Senin'),
            ('Selasa', 'Selasa'),
            ('Rabu', 'Rabu'),
            ('Kamis', 'Kamis'),
            ('Jumat', 'Jumat'),
            ('Sabtu', 'Sabtu'),
            ('Minggu', 'Minggu')   
        ]
        model = Kursus
        fields = ('deskripsi', 'lokasi', 'jadwal')
        widgets = {
            'deskripsi': forms.Textarea(attrs={'rows':5, 'placeholder':'Masukkan Deskripsi Kursus'}), 
            'lokasi': forms.TextInput(attrs={'placeholder':'Masukkan Lokasi Kursus (Contoh: Depok, Indonesia)'})
        }