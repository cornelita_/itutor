from django.apps import AppConfig


class SuntingKursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sunting_kursus'
