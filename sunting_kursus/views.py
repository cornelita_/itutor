from django.shortcuts import redirect, render
from register.models import Pengajar
from buat_kursus.models import Kursus
from .forms import SuntingKursusForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='login_logout:login_url')
def sunting_kursus(request, id_kursus):
    try:
        kursus = Kursus.objects.get(id=id_kursus)
    except Kursus.DoesNotExist:
        kursus = None
    if kursus == None or request.user.id != kursus.pengajar_id:
        return redirect('kursus_pengajar:kursus_pengajar')
    pengajar = Pengajar.objects.get(nonadmin_ptr_id=request.user.id)
    kategori = kursus.kategori
    messages = ""
    form = SuntingKursusForm(initial={
        'deskripsi':kursus.deskripsi, 
        'lokasi':kursus.lokasi, 
        'jadwal':kursus.jadwal
        })
    if request.method == 'POST':
        form = SuntingKursusForm(request.POST, initial={'pengajar': pengajar, 
            'nama_kursus':kursus.nama_kursus, 
            'kategori':kursus.kategori
        })
        if form.is_valid():
            print("cleaned data:", form.cleaned_data)
            form_clean = form.cleaned_data
            kursus.deskripsi = form_clean['deskripsi']
            kursus.lokasi = form_clean['lokasi']
            kursus.jadwal = form_clean['jadwal']
            kursus.save()
            return redirect('detail_kursus:detail_kursus', id_kursus = id_kursus)
        else:
            messages = "Anda Harus Memilih Hari Kursus"
    return render(request, "formSuntingKursus.html", {'form':form, 'kursus':kursus, 'messages':messages})
