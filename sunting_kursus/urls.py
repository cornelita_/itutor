from django.urls import path
from .views import sunting_kursus

app_name = 'sunting_kursus'
urlpatterns = [
    path('<int:id_kursus>', sunting_kursus, name='sunting_kursus'),
]
