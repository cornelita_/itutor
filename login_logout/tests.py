from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User

class LoginTest(TestCase):
    def test_login_url_using_login_page(self):
        found = resolve('/authenticate/')
        self.assertEqual(found.func, views.login_page)

    def test_logout_url_using_logout_user(self):
        found = resolve('/authenticate/logout')
        self.assertEqual(found.func, views.logout_user)

    def test_login_url_correct_data(self):
        user = User.objects.create_user(username="testing", password="maululus")
        user.save()
        response = Client().post('/authenticate/', data={
            "username": "testing",
            "password": "maululus",
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')

    def test_login_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus")
        response = c.get('/authenticate/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')

    def test_login_url_wrong_data(self):
        response = Client().post('/authenticate/', data={
            "username": "testing",
            "password": "maululus",
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('Username atau password anda belum tepat', html_contents)

    def test_logout_url(self):
        user = User.objects.create_user(username="testing", password="maululus")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus")
        response = c.get('/authenticate/logout')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')