from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

def login_page(request):
    response = {'hide_header': True, 'hide_footer': True}
    if request.user.is_authenticated:
        if request.user.is_staff:
            return redirect('verifikasi_kursus:pengajuan_url')
        else:
            return redirect('landing_page')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username = username, password = password)

            if user is not None:
                login(request, user)
                if user.is_staff:
                    return redirect('verifikasi_kursus:pengajuan_url')
                return redirect(request.POST.get('next') or 'landing_page')
            else:
                response['username'] = username
                response['message'] = 'Username atau password anda belum tepat'
        return render(request, 'login.html', response)

@login_required(login_url='login_logout:login_url')
def logout_user(request):
    logout(request)
    return redirect('landing_page')