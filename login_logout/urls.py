from django.urls import path
from . import views

app_name = 'login_logout'
urlpatterns = [
    path('', views.login_page, name="login_url"),
    path('logout', views.logout_user, name="logout_url")
]