
$(function () {
    const endpoint = '/cari-kursus/';
    var delay = false;

    let ajax_call = function (endpoint, req_parameters) {
        $.getJSON(endpoint, req_parameters)
            .done(response => {
                $("#par").fadeOut(600);
                $('.ck-parent').replaceWith(response['templates']);

            })
    };


    $('.search-button').on('click', function (event) {
        var keyword = $('#searchTerm').val();
        var category = $('#dropdownMenuButton').text();

        if (category.includes('Pilih')) {
            category = '';
        }

        const par = { keyword: keyword, category: category, };

        if (delay) clearTimeout(delay);
        
        $('.ck-parent').replaceWith(`
        <div class='ck-parent'>
            <div></div>
            <div id='par'>
                <div class="cv-spinner">
                    <span class="spinner"></span>
                </div>
            </div>
        </div>
        `);
        $("#par").fadeIn(300);　

        delay = setTimeout(ajax_call, 800, endpoint, par);

    });

    $('.dropdown-item').on('click', function (event) {
        event.preventDefault();
        var category = $(this).text();
        $('.dropdown-toggle').text(category);
    });
})