from django.apps import AppConfig


class CarikursusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'carikursus'
