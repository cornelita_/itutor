from django.http.response import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from buat_kursus.models import Kategori, Kursus
from daftar_kursus.models import Pendaftaran
from .dummy import kursus_dummy
from math import ceil

# Create your views here.
def cari_kursus(request):
    kursuses = Kursus.objects.filter(status_verifikasi=1)
    kategoris = Kategori.objects.all()
    context = {'kursuses': kursuses, 'categories': kategoris, 'ratings':[]}
    if request.is_ajax():
        keyword = request.GET['keyword']
        category = request.GET['category']

        if keyword != '' and category == '':
            filt_kursuses = kursuses.filter(nama_kursus__icontains=keyword)
            context['kursuses'] = filt_kursuses
        elif keyword == '' and category != '':
            filt_kursuses = kursuses.filter(kategori__nama_kategori=category)
            context['kursuses'] = filt_kursuses
        elif keyword != '' and category != '':
            filt_kursuses = kursuses.filter(nama_kursus__icontains=keyword).filter(kategori__nama_kategori=category)
            context['kursuses'] = filt_kursuses
        json_context = {}
        context['zipped'] = zip(context['kursuses'],  get_ratings(context['kursuses']))
        html = render_to_string(
            template_name='generateKursusCard.html',
            context = context
        )
        json_context['templates'] = html
        return JsonResponse(data=json_context)
    context['zipped'] = zip(context['kursuses'],  get_ratings(context['kursuses']))
    return render(request, "cariKursusPage.html", context)

def get_ratings(kursuses):
    list_rating = []
    for i in kursuses:
        pendaftaran = Pendaftaran.objects.filter(kursus_id=i.id).filter(status_pendaftar=3)

        rata_rating = 0
        jumlah = 0
        for r in pendaftaran:
            rata_rating += r.rating
            jumlah += 1
        if jumlah != 0 :
            rata_rating = rata_rating / jumlah

        html = render_to_string(
            template_name="generateStar.html",
            context= {
                'int_range' : range(int(rata_rating)),
                'decimal_score' : rata_rating - int(rata_rating),
                'remaining_star' : range(5 - ceil(rata_rating))
            }
        )

        list_rating.append(html)
    return list_rating