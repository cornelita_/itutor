from django.urls import path
from .views import cari_kursus

app_name = 'carikursus'
urlpatterns = [
    path('', cari_kursus, name='cari_kursus'),
]