kursus_dummy = [
    ['Kursus Bahasa Jepang', 'Achmad Fikri', 'Bahasa'],
    ['Kursus Bahasa Jawa', 'Achmad Fikri', 'Bahasa'],
    ['Kursus Pemrograman Python', 'Achmad Fikri', 'Pemrograman'],
    ['Menjadi Algorithm Designer yang Sangat Jago dan Handal', 'Achmad Fikri', 'Pemrograman'],
    ['Kursus Pemrograman Java', 'Achmad Fikri', 'Pemrograman'],
    ['Kursus Kalkulus 2', 'Achmad Fikri', 'Matematika'],
    ['Kursus Bahasa Jepang', 'Basyira Sabita', 'Bahasa'],
    ['Kursus Bahasa Jawa', 'Basyira Sabita', 'Bahasa'],
    ['Kursus Pemrograman Python', 'Nurul Mecyani', 'Pemrograman'],
]