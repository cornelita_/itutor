from django.http import response
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from register.models import NonAdmin, Pelajar
from daftar_kursus.models import Pendaftaran
from django.contrib.auth.models import User
from .forms import FeedbackForm
from django.contrib import messages

@login_required(login_url='login_logout:login_url')
def kursus_pelajar(request):
    try:
        pelajar = Pelajar.objects.get(nonadmin_ptr_id=request.user.id)
    except:
        return render(request, "peringatun.html")
    print(pelajar.id)
    pendaftaran = pelajar.pendaftaran_set.all()
    print(pendaftaran)
    return render(request, "KursusPelajarNew.html", {'pendaftaran': pendaftaran})
    

def delete_pendaftaran(request, pk):
    pendaftaran = Pendaftaran.objects.get(id=pk)
    pendaftaran.delete()
    return redirect('kursus_pelajar:kursus_pelajar')

def buat_feedback(request, id_pendaftaran):
    pendaftaran = Pendaftaran.objects.get(id=id_pendaftaran)
    form = FeedbackForm()
    print('tesssss')
    print(request.method)
    if request.method == 'POST':
        form = FeedbackForm(request.POST)
        print(form)
        print(request.POST)
        print(form.is_valid())
        if form.is_valid():
            print("masuk")
            form_clean = form.cleaned_data
            pendaftaran.feedback = form_clean['feedback']
            pendaftaran.rating = form_clean['rating']
            pendaftaran.status_pendaftar = 3
            pendaftaran.save()
            return redirect('kursus_pelajar:kursus_pelajar')
    return render(request, "KursusPelajarNew.html", {'form':form})

