from django import forms
from django.forms import ModelForm, widgets
from daftar_kursus.models import Pendaftaran


CHOICES=[(1,'1'),
         (2,'2'),
         (3,'3'),
         (4,'4'),
         (5,'5')]

class FeedbackForm(ModelForm):
    class Meta:
        model = Pendaftaran
        fields = ('feedback', 'rating')
        widgets = {
            'feedback': forms.Textarea(attrs={'rows':5, 'placeholder':'Maksimal 500 Karakter', 'label':'feedback'}),
            'rating': forms.RadioSelect(
                attrs={'class':'form-check-input','id':'inlineRadio'}) 
        }