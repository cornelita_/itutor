from django.urls import path
from .views import kursus_pelajar, delete_pendaftaran, buat_feedback

app_name = 'kursus_pelajar'
urlpatterns = [
    path('', kursus_pelajar, name='kursus_pelajar'),
    path('delete/<int:pk>', delete_pendaftaran, name='delete_pendaftaran'),
    path('update/<int:id_pendaftaran>', buat_feedback, name='buat_feedback'),
]