from django.apps import AppConfig


class KursusPelajarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kursus_pelajar'
