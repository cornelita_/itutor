from django.urls import path
from .views import kursus_pengajar, delete_kursus

app_name = 'kursus_pengajar'
urlpatterns = [
    path('', kursus_pengajar, name='kursus_pengajar'),
    path('delete/<int:pk>', delete_kursus, name='delete_kursus'),
]