from django.apps import AppConfig


class KursusPengajarConfig(AppConfig):
    name = 'kursus_pengajar'
