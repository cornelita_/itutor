from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from register.models import Pelajar, Pengajar, NonAdmin
from buat_kursus.models import Kursus
from django.contrib.auth.models import User

@login_required(login_url='login_logout:login_url')
def kursus_pengajar(request):
    try:
        pengajar = Pengajar.objects.get(nonadmin_ptr_id=request.user.id)
        kursus = pengajar.kursus_set.all()
        response = {"kursus":kursus, "pengajar":pengajar.username}
        return render(request, "kursusPengajar.html", response)
    except:
        return render(request, "peringatawn.html")

def delete_kursus(request, pk):
    kursus = Kursus.objects.get(id=pk)
    kursus.delete()
    # messages.success(request, (f"Artikel {request.GET['artikel']} berhasil dihapus!"))
    return redirect('kursus_pengajar:kursus_pengajar')


